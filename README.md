ics-ans-jupyterhub
==================

Ansible playbook to install jupyterhub.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

License
-------

BSD 2-clause
