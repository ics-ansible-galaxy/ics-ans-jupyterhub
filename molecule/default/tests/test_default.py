import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('jupyterhub')


def test_jupyterhub_index(host):
    # This tests that traefik forwards traffic to jupyterhub
    # and that we can access the jupyterhub index page
    cmd = host.run('curl -H Host:ics-ans-jupyterhub-default -k -L https://localhost')
    assert '<title>JupyterHub</title>' in cmd.stdout
